package com.mobicare.colaborador.service;



import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobicare.colaborador.library.BaseService;
import com.mobicare.colaborador.model.BlackList;
import com.mobicare.colaborador.model.Colaborador;
import com.mobicare.colaborador.repository.ColaboradorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Calendar;
import java.util.Date;

/** Somente é permitido até 20% de colaboradores menores que 18 anos por setor.
 Somente é permitido até 20% de colaboradores maiores que 65 anos na empresa.
 * @author Marcelo Salvatori
 * @return true
 */


@Service
public class ColaboradorService extends BaseService<Colaborador, ColaboradorRepository, Long> {

    @Autowired
    ColaboradorRepository colaboradorRepository;




    @Override
    public Colaborador save(Colaborador item) {

        if  (validaIdade(item.getDtnascimento()) &&  (validaBlackList(item.getCpf()))){
            return super.save(item);
        } else
        return null;
    }
    public boolean validaIdade(Date date){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        Date date2 = new Date();
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);

        int idade = calendar2.get(Calendar.YEAR) - calendar.get(Calendar.YEAR);

        long idade18    = 0;
        long idade65    = 0;
        long qtdpercent = 0;
        long qtd        = 0;

        if ((idade < 18) || (idade > 65)) {
            if (idade < 18){
                idade18 =  colaboradorRepository.findIdade18();
                idade18 = idade18 + 1;
            } else if (idade > 65) {
                idade65 =  colaboradorRepository.findIdade65();
                idade65 = idade65 + 1;
            }
            qtd =  colaboradorRepository.findQtd();
            qtdpercent  = (qtd * 20) / 100;
            if ((idade18 > qtdpercent) || (idade65 > qtdpercent))
                return false;
            else
                return true;
        }else
            return true;
    }
    public boolean validaBlackList(String cpf) {

         final String uri = "https://5e74cb4b9dff120016353b04.mockapi.io/api/v1/blacklist?search="+cpf;
         BlackList blackList = new BlackList();
         RestTemplate restTemplate = new RestTemplate();
         ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
         ObjectMapper mapper = new ObjectMapper();
         String json = response.getBody();
        BlackList[] blackLists= new BlackList[0];
        try {
            blackLists = mapper.readValue(json, BlackList[].class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return (blackLists.length >= 1 ? false :true );

    }

    public Page<Colaborador> findAllColaboradoresSetores(Pageable pageable) {
        return colaboradorRepository.findAllcolaboradorSetor(pageable);
    }
}
