package com.mobicare.colaborador.model;

import com.mobicare.colaborador.interfaces.DTOEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;

@JsonPropertyOrder({"id","descricao"})
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SetorDto extends RepresentationModel<SetorDto> implements Serializable, DTOEntity {

    private static final long serialVersionUID = -1640411494002006916L;

    @JsonProperty("id")
    private Long id;

    @JsonProperty("descricao")
    private String descricao;


}
