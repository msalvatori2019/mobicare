package com.mobicare.colaborador.model;


import com.mobicare.colaborador.interfaces.DTOEntity;
import com.mobicare.colaborador.library.BaseBean;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="colaborador")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@SequenceGenerator(name = "seq_colaborador", sequenceName = "seq_colaborador",  allocationSize = 10)
public class Colaborador extends BaseBean<Long> implements Serializable, DTOEntity {

    private static final long serialVersionUID = -742276256974237621L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_colaborador")
    private Long id;

    @Length(min = 1, max = 16, message = "cpf deve conter entre 1 e 16  caracteres")
    @Column(name = "cpf",nullable = false)
    private String cpf;


    @Length(min = 1, max = 200, message = "Nome deve conter entre 1 e 200  caracteres")
    @Column(name = "nome",nullable = false)
    private String nome;

    @Length(min = 1, max = 11, message = "Telefone deve conter entre 1 e 11  caracteres")
    @Column(name = "telefone",nullable = false)
    private String telefone;


    @Length(min = 1, max = 200, message = "e-mail deve conter entre 1 e 200  caracteres")
    @Column(name = "email",nullable = false)
    private String email;


    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "dtnascimento")
    private Date dtnascimento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_setor" , referencedColumnName = "id")
    private Setor setor;


    @Override
    public Long getIdEntity() {
        return  this.id;
    }

}
