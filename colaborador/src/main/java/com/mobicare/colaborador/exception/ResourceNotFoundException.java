package com.mobicare.colaborador.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException  extends RuntimeException{


    private static final long serialVersionUID = 2243719475087666820L;

    public  ResourceNotFoundException(String exception){
        super(exception);
    }
}
