package com.mobicare.colaborador.repository;

import com.mobicare.colaborador.model.Colaborador;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColaboradorRepository extends PagingAndSortingRepository<Colaborador, Long> {

    @Query(value = "select count(*) qtg from Colaborador  where extract(year from age(dtnascimento))<18", nativeQuery = true)
    long findIdade18();

    @Query(value = "select count(*) qtg from Colaborador  where extract(year from age(dtnascimento))>65", nativeQuery = true)
    long findIdade65();

    @Query(value = "select count(*) qtg from Colaborador ", nativeQuery = true)
    long findQtd();

    @Query("select  c from Colaborador c order by c.setor.descricao")
    Page<Colaborador> findAllcolaboradorSetor(Pageable pageable);

}

