package com.mobicare.colaborador.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.mobicare.colaborador.exception.ResourceNotFoundException;
import com.mobicare.colaborador.interfaces.IrepositoryTests;
import com.mobicare.colaborador.model.Colaborador;
import com.mobicare.colaborador.model.Setor;
import com.mobicare.colaborador.repository.ColaboradorRepository;
import com.mobicare.colaborador.repository.SetorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ColaboradorValidaServiceTests implements IrepositoryTests {

    @Autowired
    SetorRepository setorRepository;

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @Autowired
    ColaboradorService colaboradorService;

    Setor setor;

    @Transactional
    @Test
    @Override
    public void saveOrUpdate() throws ParseException, JsonProcessingException {
    /**Inclusao Setor
     * @author Marcelo Salvatori
     * @param descricao - descricao do setor
     * @return Setor - Valor do resultado da inclusão
     */
        this.setor = new Setor();
        this.setor.setDescricao("Setor A");
        this.setor.setId(1L);

        this.setor = setorRepository.save(this.setor);
        assertThat(this.setor.getId()).isNotNull();

        Colaborador colaborador =  new Colaborador();
        colaborador = new Colaborador();
        colaborador.setCpf("88888888");
        colaborador.setNome("Fulano 1");
        colaborador.setTelefone("99999999");
        colaborador.setEmail("m.mmm@mmmm.mmm.nn");
        String data = "10/10/2004";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        colaborador.setDtnascimento(sdf.parse(data));
        colaborador.setSetor(setor);
        colaboradorRepository.save(colaborador);


        Colaborador colaborador2 =  new Colaborador();
        colaborador2 = new Colaborador();
        colaborador2.setCpf("88888888");
        colaborador2.setNome("Fulano 2");
        colaborador2.setTelefone("99999999");
        colaborador2.setEmail("m.mmm@mmmm.mmm.nn");
        String data2 = "10/10/2000";
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
        colaborador2.setDtnascimento(sdf.parse(data2));
        colaborador2.setSetor(setor);
        colaboradorRepository.save(colaborador2);

        Colaborador colaborador3 =  new Colaborador();
        colaborador3 = new Colaborador();
        colaborador3.setCpf("88888888");
        colaborador3.setNome("Fulano 3");
        colaborador3.setTelefone("99999999");
        colaborador3.setEmail("m.mmm@mmmm.mmm.nn");
        String data3 = "10/10/2000";
        SimpleDateFormat sdf3 = new SimpleDateFormat("dd/MM/yyyy");
        colaborador3.setDtnascimento(sdf.parse(data2));
        colaborador3.setSetor(setor);
        colaboradorRepository.save(colaborador3);

        Colaborador colaborador4 =  new Colaborador();
        colaborador4 = new Colaborador();
        colaborador4.setCpf("88888888");
        colaborador4.setNome("Fulano 4");
        colaborador4.setTelefone("99999999");
        colaborador4.setEmail("m.mmm@mmmm.mmm.nn");
        String data4 = "10/10/2000";
        SimpleDateFormat sdf4 = new SimpleDateFormat("dd/MM/yyyy");
        colaborador4.setDtnascimento(sdf.parse(data4));
        colaborador4.setSetor(setor);
        colaboradorRepository.save(colaborador4);

        Colaborador colaborador5 =  new Colaborador();
        colaborador5 = new Colaborador();
        colaborador5.setCpf("77386499094");
        colaborador5.setNome("Fulano 5");
        colaborador5.setTelefone("99999999");
        colaborador5.setEmail("m.mmm@mmmm.mmm.nn");
        String data5 = "10/10/2005";
        SimpleDateFormat sdf5 = new SimpleDateFormat("dd/MM/yyyy");
        colaborador5.setDtnascimento(sdf.parse(data5));
        colaborador5.setSetor(setor);

        colaboradorService.validaIdade(colaborador5.getDtnascimento());
        assertThat(colaboradorService.validaIdade(colaborador5.getDtnascimento())).isEqualTo(false);

        assertThat(colaboradorService.validaBlackList(colaborador5.getCpf())).isEqualTo(false);

        assertThat(colaboradorService.save(colaborador5)).isNull();
    }
}
