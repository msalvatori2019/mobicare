package com.mobicare.colaborador.repository;

import com.mobicare.colaborador.interfaces.IrepositoryTests;
import com.mobicare.colaborador.model.Colaborador;
import com.mobicare.colaborador.model.Setor;
import com.mobicare.colaborador.service.ColaboradorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ColaboradorRepositoryTests implements IrepositoryTests {

    @Autowired
    SetorRepository setorRepository;

    @Autowired
    ColaboradorRepository colaboradorRepository;

    @Autowired
    ColaboradorService colaboradorService;

    Setor setor;

    @Transactional
    @Test
    @Override
    public void saveOrUpdate() throws ParseException {

        /**Inclusao Setor
         * @author Marcelo Salvatori
         * @param descricao - descricao do setor
         * @return Setor - Valor do resultado da inclusão
         */
        this.setor = new Setor();
        this.setor.setDescricao("Setor A");
        this.setor.setId(1L);

        this.setor = setorRepository.save(this.setor);
        assertThat(this.setor.getId()).isNotNull();

        /**Inclusao Colaborador
         * @author Marcelo Salvatori
         * @param cpf - cpf do colaborador
         * @param nome - nome do colaborador
         * @param telefone - telefone do colaborador
         * @param email - email do colaborador
         * @param Dtnascimento -  data nascimento  do colaborador
         * @param Setor -  setor aonde o colaborador esta alocado
         * @return Colaborador - Valor do resultado da inclusão
         */

        Colaborador colaborador =  new Colaborador();
        colaborador = new Colaborador();
        colaborador.setCpf("88888888");
        colaborador.setNome("Fulano 1");
        colaborador.setTelefone("99999999");
        colaborador.setEmail("m.mmm@mmmm.mmm.nn");
        String data = "10/10/1980";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        colaborador.setDtnascimento(sdf.parse(data));
        colaborador.setSetor(setor);
        colaborador = colaboradorRepository.save(colaborador);
        assertThat(colaborador.getId()).isNotNull();

        /** Alteração Colaborador
         * @author Marcelo Salvatori
         * @param cpf - cpf do colaborador
         * @param nome - nome do colaborador
         * @param telefone - telefone do colaborador
         * @param email - email do colaborador
         * @param Dtnascimento -  data nascimento  do colaborador
         * @param Setor -  setor aonde o colaborador esta alocado
         * @return Colaborador - Valor do resultado da alteração
         */

        Optional<Colaborador> colaboradoropt = colaboradorRepository.findById(colaborador.getId());
        Colaborador colaborador1 = new Colaborador();
        colaborador1 = colaboradoropt.get();
        colaborador1.setNome("Fulano 2");
        Colaborador colaborador2 = new Colaborador();
        colaborador2 = colaborador1;
        colaborador = colaboradorRepository.save(colaborador1);
        assertThat(colaborador2).isEqualTo(colaborador);

        /**Exclusão da Colaborador e validação
         * @author Marcelo Salvatori
         * @return true
         */
        colaboradorRepository.delete(colaborador);
        Optional<Setor> setornull = setorRepository.findById(colaborador2.getId());
        assertThat(setornull.isEmpty());


    }
}
